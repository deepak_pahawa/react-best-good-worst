/* eslint-disable no-use-before-define */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  root: {
    width: 300,
    '& > * + *': {
      marginTop: theme.spacing(3),
    },
  },
}));

export const StartDateView = ({date, onDateSelection}) => {
  const classes = useStyles();

  return (
      <form className={classes.container} noValidate>
        <TextField
          id="datetime-local"
          label="Next appointment"
          value={date}
          type="datetime-local"
          onChange={onDateSelection}
          // defaultValue="2017-05-24T10:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
        />
      </form>
  );
}