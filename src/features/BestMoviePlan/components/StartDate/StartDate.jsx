/* eslint-disable no-use-before-define */
import React from 'react';
import { StartDateView } from './StartDate.view';


export const StartDate = ({date, onSelection}) => {
  const onDateSelection = (event) => { event.persist();  onSelection(event.target.value) }

  return (
      <StartDateView onDateSelection={onDateSelection} date={date}/>
  );
}