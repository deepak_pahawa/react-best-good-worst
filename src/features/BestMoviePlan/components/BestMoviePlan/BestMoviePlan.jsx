/* eslint-disable no-use-before-define */
import React, { useMemo, useState, useCallback } from 'react';
import { BestMoviePlanView } from './BestMoviePlan.view';

export const BestMoviePlan = () => {

  const [selectedCountries, setSelectedCountries] = useState([])
  const [date, setDate] = useState("")


  const onChangeHandler = useCallback((_, value) => {
    setSelectedCountries(value);
  },[]);

  const manipulatedData = useMemo(() => {
    if(!selectedCountries) return null;
    return selectedCountries.map(country => {
      country.location = true
      return country
    }
    )
  }, [selectedCountries])


  return (

    <BestMoviePlanView onChangeHandler={onChangeHandler} date={date} onDateSelection={setDate} manipulatedData={manipulatedData}/>
  );
}



