/* eslint-disable no-use-before-define */
import React, { useState} from 'react';
import { MovieSelector } from '../MovieSelector/MovieSelector';
import { StartDate } from '../StartDate/StartDate';
import {CardStyle } from '../../../../styles/CardStyle'

export const BestMoviePlanView = ({onChangeHandler, date, onDateSelection, manipulatedData }) => {
    const [iAgree, setIAgree] = useState(false)
  return (
    <CardStyle>
      <h4>Best Approach</h4>
      <MovieSelector updateSelectedCountries={onChangeHandler} />
      <br />
      <StartDate date={date} onSelection={onDateSelection} manipulatedData={manipulatedData} />
      <br />
      <label htmlFor="check">
        <input type="checkbox" name="check" value={iAgree} onChange={() => setIAgree(state => !state)} />I Agree</label>
    </CardStyle>
  );
}



