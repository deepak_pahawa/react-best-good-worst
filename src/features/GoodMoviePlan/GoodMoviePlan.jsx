/* eslint-disable no-use-before-define */
import React, { useState } from 'react';
import { MovieSelector } from './AutoComplete/MovieSelector';
import { StartDate } from './StartDate/StartDate';
import {CardStyle} from '../../styles/CardStyle'


export const GoodMoviePlan = () => {

  const [, setSelectedCountries] = useState([])
  const [iAgree, setIAgree] = useState(false)
  const [date, setDate] = useState("")

  const onDateSelection = (date) => { setDate(date) }
  const onChangeHandler = (_, value) => {
    setSelectedCountries(value);
  }


  return (

    <CardStyle>
      <h4>Good Approach</h4>
      <MovieSelector updateSelectedCountries={onChangeHandler}/>
      <br/>
      <StartDate date={date} onSelection={onDateSelection}/>
      <br/>
      <label htmlFor="check">
        <input type="checkbox" name="check" value={iAgree} onChange={() => setIAgree(state => !state)} />I Agree</label>
    </CardStyle>
  );
}



