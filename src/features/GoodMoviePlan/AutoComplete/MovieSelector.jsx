/* eslint-disable no-use-before-define */
import React, { useState } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { top100Films } from '../../../data/data'

const useStyles = makeStyles((theme) => ({
  root: {
    width: 300,
    '& > * + *': {
      marginTop: theme.spacing(3),
    },
  },
}));

export const MovieSelector = ({updateSelectedCountries}) => {
  const classes = useStyles();

  const [selectedCountries, setSelectedCountries] = useState([])

  const onChangeHandler = (_, value) => {
    setSelectedCountries(value);
  }

  const onBlurHandler = () => {
    console.log(selectedCountries);
    updateSelectedCountries(selectedCountries)
  }


  return (
      <div className={classes.root}>
        <Autocomplete
          multiple
          id="tags-standard"
          options={top100Films}
          onChange={onChangeHandler}
          onBlur={onBlurHandler}
          getOptionLabel={(option) => option.title}
          defaultValue={[top100Films[13]]}
          renderInput={(params) => (
            <TextField
              {...params}
              variant="standard"
              label="Multiple values"
              placeholder="Favorites"
            />
          )}
        />
      </div>
  );
}



