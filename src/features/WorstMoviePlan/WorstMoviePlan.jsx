/* eslint-disable no-use-before-define */
import React, { useState } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { top100Films } from '../../data/data';
import {CardStyle} from '../../styles/CardStyle';

const useStyles = makeStyles((theme) => ({
  root: {
    width: 300,
    '& > * + *': {
      marginTop: theme.spacing(3),
    },
  },
}));

export const WorstMoviePlan = () => {
  const classes = useStyles();

  const [, setSelectedCountries] = useState([])
  const [iAgree, setIAgree] = useState(false)
  const [, setDate] = useState("")

  const onDateSelection = (event) => { event.persist(); setDate(event.target.value) }
  const onChangeHandler = (_, value) => {
    setSelectedCountries(value);
  }


  return (

    <CardStyle >
      <h4>Worst Approach</h4>
      <div className={classes.root}>
        <Autocomplete
          multiple
          id="tags-standard"
          options={top100Films}
          onChange={onChangeHandler}
          getOptionLabel={(option) => option.title}
          defaultValue={[top100Films[13]]}
          renderInput={(params) => (
            <TextField
              {...params}
              variant="standard"
              label="Multiple values"
              placeholder="Favorites"
            />
          )}
        />
      </div>
      <br/>
      <form className={classes.container} noValidate>
        <TextField
          id="datetime-local"
          label="Next appointment"
          type="datetime-local"
          onChange={onDateSelection}
          defaultValue="2017-05-24T10:30"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
        />
      </form>
          <br/>
      <label htmlFor="check">
        <input type="checkbox" name="check" value={iAgree} onChange={() => setIAgree(state => !state)} />I Agree</label>
    </CardStyle>
  );
}



