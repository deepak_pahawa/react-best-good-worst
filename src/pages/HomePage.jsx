import { WorstMoviePlan } from '../features/WorstMoviePlan/WorstMoviePlan';
import { GoodMoviePlan } from '../features/GoodMoviePlan/GoodMoviePlan';
import { BestMoviePlan } from '../features/BestMoviePlan';


export const HomePage = () => {
  return (
      <>
      <WorstMoviePlan />
      <GoodMoviePlan />
      <BestMoviePlan />
      </>
  );
}
